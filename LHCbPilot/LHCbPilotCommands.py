###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" LHCb-specific pilot commands
"""
from __future__ import absolute_import, division, print_function

import json
import os
import random
import sys
import timeit

############################
# python 2 -> 3 "hacks"
try:
    from Pilot.pilotCommands import (
        ConfigureArchitecture,
        ConfigureBasics,
        InstallDIRAC,
    )
    from Pilot.pilotTools import CommandBase, safe_listdir
except ImportError:
    from pilotCommands import (
        ConfigureArchitecture,
        ConfigureBasics,
        InstallDIRAC,
    )
    from pilotTools import CommandBase, safe_listdir

# Compatibility imports for HTTP requests
try:
    # For Python 3
    from urllib.request import urlopen
except ImportError:
    # For Python 2
    from urllib2 import urlopen

############################


# LHCb pilot commands

try:
    from os import getxattr
except ImportError:
    def getxattr(file_path, attribute_name):
        """Get the value of a specific extended attribute."""
        import ctypes, errno

        buffer_size = 1024
        buffer = ctypes.create_string_buffer(buffer_size)

        libc = ctypes.CDLL(None, use_errno=True)

        result = libc.getxattr(file_path.encode(), attribute_name.encode(), buffer, buffer_size)
        if result < 0:
            raise OSError("Error retrieving attribute value", errno.errorcode[ctypes.get_errno()])

        return buffer.raw[:result]


class LHCbCommandBase(CommandBase):
    """Simple extension, just for LHCb parameters"""

    def __init__(self, pilotParams):
        """c'tor"""
        super(LHCbCommandBase, self).__init__(pilotParams)
        pilotParams.localConfigFile = "pilot.cfg"
        pilotParams.architectureScript = "dirac-architecture"
        pilotParams.preinstalledEnvPrefix = '/cvmfs/lhcb.cern.ch/lhcbdirac/'
        pilotParams.CVMFS_locations = [
            '/cvmfs/lhcb.cern.ch/',
            '/cvmfs/lhcbdev.cern.ch/',
            '$VO_LHCB_SW_DIR',
        ]


class LHCbInstallDIRAC(LHCbCommandBase, InstallDIRAC):
    """Try to source lhcbdirac, and fall back to dirac-install when the requested version is not in CVMFS.
    When we reach here we expect to know the release version to install
    """

    def execute(self):
        """Standard module executed"""
        environment = os.environ.copy()
        if "LHCb_release_area" not in environment:
            environment["LHCb_release_area"] = "/cvmfs/lhcb.cern.ch/lib/lhcb/"
        self.pp.installEnv = environment

        if "lbRunOnly" in self.pp.genericOption:
            # Adapt to vanilla Pilot option name
            self.pp.genericOption = "cvmfsOnly"

        super(LHCbInstallDIRAC, self).execute()


class LHCbTestDiracX(LHCbCommandBase):
    """ Just a class to test the response from the lhcbdiracx service
    """

    def execute(self):
        VAR_PERCENTAGE = 20  # CHANGE ME!
        VAR_NUMBER_OF_CALLS = 1  # CHANGE ME!

        self.delays = []  # Store return values here

        # ping-ing
        if random.randrange(100) <= VAR_PERCENTAGE:
            self.log.info("Ping the lhcbdiracx server")
            execution_time = timeit.timeit(self._ping_wrapper, number=VAR_NUMBER_OF_CALLS)
            self.log.info("Execution time: {:.6f} seconds".format(execution_time))

            # Log the recoded delays
            for delay in self.delays:
                self.log.info("delay recorded: {}".format(delay))

        # getting a token
        if random.randrange(100) <= VAR_PERCENTAGE:
            self.log.info("Getting a token from the lhcbdiracx server")
            execution_time = timeit.timeit(self._token_wrapper, number=VAR_NUMBER_OF_CALLS)
            self.log.info("Execution time: {:.6f} seconds".format(execution_time))

            # Log the recoded delays
            for delay in self.delays:
                self.log.info("delay recorded: {}".format(delay))

    def _ping(self, route="ping"):
        # call the lhcbdiracx scaling_test route
        url = "https://lhcbdiracx-cert.app.cern.ch/api/scaling_test/%s" % route
        try:
            response = urlopen(url)
            # Optional: check response status
            if response.getcode() == 200:
                self.log.info("%s successful" % route)
                response_output = response.read()
                if isinstance(response_output, bytes):
                    response_output = response_output.decode("utf-8")
                delay = json.loads(response_output)['delay']
                return delay
            else:
                self.log.error("Operation failed with status code: {}".format(response.getcode()))
        except Exception as e:
            self.log.error("Error scale-testing the server: {}".format(e))

    def _ping_wrapper(self):
        # Call _ping and store its result
        result = self._ping()
        self.delays.append(result)

    def _token_wrapper(self):
        # Call _token and store its result
        result = self._ping(route="token")
        self.delays.append(result)

class LHCbRequireApptainer(CommandBase):
    """Require that apptainer can be used by SingularityCE.

    This command must be ran after (LHCb)InstallDIRAC.
    """

    def execute(self):
        cmd = "apptainer exec --contain --ipc"
        cmd += " --home /tmp --userns --bind /cvmfs"
        cmd += " --bind " + os.getcwd()
        cmd += " /cvmfs/cernvm-prod.cern.ch/cvm4"
        cmd += " bash -c 'echo PWDPWDPWD=$PWD=PWDPWDPWD'"
        retCode, cmdOut = self.executeAndGetOutput(cmd, self.pp.installEnv)
        if retCode:
            self.log.error("Apptainer failed to run with [ERROR %d] cmdOut=" % retCode)
            self.log.error(cmdOut)
            retCode, cmdOut = self.executeAndGetOutput("lb-debug-platform --debug", self.pp.installEnv)
            self.log.error("Output of lb-debug-platform:\n%s" % cmdOut)
            self.exitWithError(1)
        # To validate apptainer is working we check the current directory in the container is as expected
        if "PWDPWDPWD=/tmp=PWDPWDPWD" not in cmdOut:
            self.log.error(
                "Apptainer did not start in the expected directory %r" % (cmdOut,)
            )
            retCode, cmdOut = self.executeAndGetOutput("lb-debug-platform --debug", self.pp.installEnv)
            self.log.error("Output of lb-debug-platform:\n%s" % cmdOut)
            self.exitWithError(1)
        self.log.info("Apptainer is working as expected")


class LHCbConfigureBasics(LHCbCommandBase, ConfigureBasics):
    """Only case here, for now, is if to set or not the CAs and VOMS location, that should be found in CVMFS"""

    def _getBasicsCFG(self):
        """Fill in the sharedArea"""
        super(LHCbConfigureBasics, self)._getBasicsCFG()

        # Adding SharedArea (which should be in CVMFS)
        if "VO_LHCB_SW_DIR" in os.environ:
            sharedArea = os.path.join(os.environ["VO_LHCB_SW_DIR"], "lib")
            self.log.debug("Using VO_LHCB_SW_DIR at '%s'" % sharedArea)
            if os.environ["VO_LHCB_SW_DIR"] == ".":
                if not os.path.isdir("lib"):
                    os.mkdir("lib")
        else:
            sharedArea = "/cvmfs/lhcb.cern.ch/lib"
            self.log.warn("Can't find shared area, forcing it to %s" % sharedArea)

        self.cfg.append("-o /LocalSite/SharedArea=%s" % sharedArea)


class LHCbCleanPilotEnv(LHCbConfigureBasics):
    """Delete the pilot.cfg and the pilot.json, needed for VMs.
    Force the use of the CS given by command line. The command avoids the use of the CS server address (lhcb-conf2)
    which would not work for some resources, e.g. BOINC.
    """

    def _getBasicsCFG(self):
        super(LHCbCleanPilotEnv, self)._getBasicsCFG()
        if os.path.exists(self.pp.localConfigFile):
            os.remove(self.pp.localConfigFile)
        localPilotCFGFile = self.pp.pilotCFGFile + "-local"
        if os.path.exists(localPilotCFGFile):
            os.remove(localPilotCFGFile)
        self.cfg.append(" -o /DIRAC/Configuration/Servers=%s" % self.pp.configServer)


class LHCbAddCVMFSTags(LHCbCommandBase):
    """Looks into cvmfs locations, discover which ones are available, and add them to pp.tags.
    This command should be run before command CheckWNCompatibilities,
    which will add the tag to the configuration file.
    """

    def execute(self):
        required_locations = [
            "/cvmfs/lhcb.cern.ch/",
            "/cvmfs/lhcb-condb.cern.ch/",
            "/cvmfs/cernvm-prod.cern.ch/",
        ]
        for location in required_locations:
            if safe_listdir(location):
                self.pp.tags.append(location)
                try:
                    for key in ["user.revision", "user.root_hash"]:
                        value = getxattr(location, key).decode()
                        self.log.info("For %s %s=%r" % (location, key, value))
                except Exception as e:
                    self.log.error("Failed to list xattrs for %s: %s" % (location, e))
            else:
                self.log.error("Required CVMFS location not mounted: %s" % (location,))
                sys.exit(1)

        optional_locations = [
            "/cvmfs/lhcbdev.cern.ch/",
            "/cvmfs/unpacked.cern.ch/",
        ]
        for location in optional_locations:
            if safe_listdir(location):
                self.pp.tags.append(location)
            else:
                self.log.warn("CVMFS location not mounted: %s" % (location,))


class LHCbConfigureArchitecture(LHCbCommandBase, ConfigureArchitecture):
    """just sets the CMTCONFIG variable"""

    def execute(self):
        """calls the superclass execute and then sets the CMTCONFIG variable with the host binary tag"""

        # This sets the DIRAC architecture
        super(LHCbConfigureArchitecture, self).execute()

        # Now we find the host binary tag
        cfg = ["--BinaryTag"]
        if self.pp.useServerCertificate:
            cfg.append("-o  /DIRAC/Security/UseServerCertificate=yes")
        if self.pp.localConfigFile:
            cfg.append("--cfg")
            cfg.append(self.pp.localConfigFile)  # this file is as input

        binaryTagCmd = "%s %s" % (self.pp.architectureScript, " ".join(cfg))

        retCode, binaryTag = self.executeAndGetOutput(binaryTagCmd, self.pp.installEnv)
        if retCode:
            self.log.error("There was an error getting the binary tag [ERROR %d]" % retCode)
            self.exitWithError(retCode)
        self.log.info("BinaryTag determined: %s" % binaryTag)

        self.log.info("Setting variable CMTCONFIG=%s" % binaryTag)
        os.environ["CMTCONFIG"] = binaryTag

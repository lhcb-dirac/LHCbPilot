#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

mkdir /tmp/work
cd /tmp/work
mkdir /tmp/home
export HOME=/tmp/home

export DIRACSETUP=LHCb-Certification
export VO=LHCb
export PILOTCFG=pilot.cfg
export DIRACSE=CERN-SWTEST
export JENKINS_QUEUE=jenkins-queue_not_important
export JENKINS_SITE=DIRAC.Jenkins.ch
export DIRACUSERDN='/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=cburr/CN=761704/CN=Chris Burr'
export DIRACUSERROLE=lhcb_prmgr

export pytest_args=${pytest_args-}
export projectVersion=${projectVersion-'11.0.0a3'}
export Pilot_repo=${Pilot_repo-'DIRACGrid'}
export Pilot_branch=${Pilot_branch-'devel'}
export LHCbPilot_repo=${LHCbPilot_repo-''}
export LHCbPilot_branch=${LHCbPilot_branch-''}
export DIRAC_test_repo=${DIRAC_test_repo-'DIRACGrid'}
export DIRAC_test_branch=${DIRAC_test_branch-'integration'}
export LHCbDIRAC_test_repo=${LHCbDIRAC_test_repo-'lhcb-dirac'}
export LHCbDIRAC_test_branch=${LHCbDIRAC_test_branch-'devel'}
export JENKINS_CE=${JENKINS_CE-'jenkins.cern.ch'}
export modules=${modules-"https://gitlab.cern.ch/${LHCbDIRAC_test_repo}/LHCbDIRAC.git:::LHCbDIRAC:::${LHCbDIRAC_test_branch},https://github.com/${DIRAC_test_repo}/DIRAC.git:::DIRAC:::${DIRAC_test_branch}"}
export pilot_options=${pilot_options-'-o diracInstallOnly'}
export CSURL=${CSURL-'https://lhcb-cert-conf-dirac.cern.ch:9135/Configuration/Server'}

export WORKSPACE=$PWD
mkdir TestCode
cd TestCode
git clone "https://github.com/${Pilot_repo}/Pilot.git" -b "${Pilot_branch}"
git clone "https://github.com/${DIRAC_test_repo}/DIRAC.git" -b "${DIRAC_test_branch}"
git clone "https://gitlab.cern.ch/${LHCbPilot_repo}/LHCbPilot.git" -b "${LHCbPilot_branch}"
git clone "https://gitlab.cern.ch/${LHCbDIRAC_test_repo}/LHCbDIRAC.git" -b "${LHCbDIRAC_test_branch}"
cd ..

mkdir -p "/tmp/home/certs"
echo "${LHCBDIRAC_CERTIF_HOSTCERT}" | base64 -d > "/tmp/home/certs/hostcert.pem"
echo "${LHCBDIRAC_CERTIF_HOSTKEY}" | base64 -d > "/tmp/home/certs/hostkey.pem"
sed -i 's@/home/dirac/certs@/tmp/home/certs@g' TestCode/Pilot/tests/CI/pilot_ci.sh

# The pilot CI scipts assume the CAs are already trusted
export X509_CERT_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/certificates

bash -c 'source "$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh" && fullPilot'

set +u
cd "$WORKSPACE/PilotInstallDIR"
source "bashrc"
source "diracos/diracosrc"
source "$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh"
downloadProxy
lb-debug-platform

pip install pytest-forked pytest-xdist[psutil]
pytest "$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/" $pytest_args
